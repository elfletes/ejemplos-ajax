<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ejemplos de Ajax</title>
</head>
<!-- Libreria de jquery -->
<script
src="https://code.jquery.com/jquery-2.2.4.js"
integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2 col-md-2"></div>
            <div class="col-xs-8 col-md-8 page-header">
                <h1>Ejemplos de como usar ajax
                    <small> Ahora con CSS <span title="Avalado por mi" class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span> </small>
                </h1>
            </div>
            <div class="col-xs-2 col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-xs-2 col-md-2"></div>
            <div class="col-xs-8 col-md-8">
                <label for="name">Aqui vendra el texto de respuesta</label>
                <textarea name="name" class="form-control respuestas" rows="3" cols="50"></textarea>
            </div>
            <div class="col-xs-2 col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-xs-2 col-md-2"></div>
            <div class="col-xs-8 col-md-8">
                <div class="btn-group-vertical" role="group" aria-label="...">
                    <button type="button" class="btn btn-default sencillo_get" name="sencillo_get">Sencillo GET</button>
                    <button type="button" class="btn btn-default sencillo_ajax" name="sencillo_ajax">Sencillo Ajax</button>
                </div>
            </div>
            <div class="col-xs-2 col-md-2"></div>
        </div>

    </div>

</body>
<script type="text/javascript">
$('.sencillo_get').on('click',function(){
    //comienza el ajax, en este caso sera un "clasico" y sencillo get
    /*
    Esta funcion de jquery es la mas sencilla, ya que solo "trae" el contenido
    de lo que se encuentre en el archivo "sencillo.php"
    */
    $.get( "ajax/sencillo_get.php", function( data ) {
        $( ".respuestas" ).val( data );
    });

});
$('.sencillo_ajax').on('click',function(){

    /*
    En esta funcion ya se usa el "ajax" como debe de ser, donde le podemos pasar
    parametros en una variable que por "estandar" siempre es data, en
    "sencillo.php"
    */
    //data por lo regular es un array, con uno o varios parametros
    var data = {'cadena': 'Esto se imprimira en el campo de la respuestas'};

    $.ajax({
        method: 'POST',//aqui pude ser POST, PUT, o GET, depende de como lo quieras procesar en el archivo de respuestas del ajax
        url: 'ajax/sencillo_ajax.php', //ruta al archivo
        data: data //pasamos el array de parametros
    })
    .done(function (data) {
        //esta funcion es cuando el ajax termina su ejecucion y/o tiene el resultado del archivo de respuesta
        /*
        Para pruebas siempre hacemos un console.log, aqui podemos ver que es lo que trae la respuesta,
        y corroborar que si es lo que queriamos
        */
        console.log(data);
        $( ".respuestas" ).val( data );
    })
    .fail(function (request, status) {
        /*
        si algo falla en el archivo de respuesta, esta funcion se encarga de
        cachar el error y "imprmimos" en la consola
        */
        console.log('error status:'+status+'|error request:'+request);
    })
    .always(function () {
        /*
        esta funcion casi no se usa pero "siempre" deberia de estar, ya que como su nombre
        lo dice "siempre" se ejecuta, ya sea un .fail o un .done, por lo regular
        se usa cuando pones una animacion (loading) y al retornar (done o fail)
        se ejecuta esta funcion
        */
    });
});

</script>
</html>
